import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tony on 13.09.2017.
 */
public class RegExample {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile
                ("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
        Matcher matcher1 = pattern1.matcher("10.10.10");
        Matcher matcher2 = pattern1.matcher("10.10.1.1");
        Matcher matcher3 = pattern1.matcher("222.222.2.999");
        Matcher matcher4 = pattern1.matcher("22.2222.22.2");
        Matcher matcher5 = pattern1.matcher("13.51.0.d");
        Matcher matcher6 = pattern1.matcher("192.168.1.1");
        System.out.println(matcher1.find());
        System.out.println(matcher2.find());
        System.out.println(matcher3.find());
        System.out.println(matcher4.find());
        System.out.println(matcher5.find());
        System.out.println(matcher6.find());

    }
}
