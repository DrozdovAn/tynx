import java.util.Arrays;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by Tony on 20.09.2017.
 */
public class Reg3 {
    public static void main(String[] args) {
        Random rand = new Random();
        String[] numbers = new String[10];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.toString(rand.nextInt(Integer.MAX_VALUE));
        }
        System.out.println(Arrays.toString(numbers));

        String regex = "([02468]{3,5})$";

        int count = 0;
        for (String string : numbers) {
            if(Pattern.matches(regex, string)) count++;
        }
        System.out.println(count);
    }
}
